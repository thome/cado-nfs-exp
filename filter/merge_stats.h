#ifndef MERGE_STATS_H_
#define MERGE_STATS_H_

#ifdef __cplusplus
extern "C" {
#endif

void merge_stat_add1(int id, int weight);
void merge_stat_prepare(int nthreads);
void merge_stat_print_stats();

#ifdef __cplusplus
}
#endif

#endif	/* MERGE_STATS_H_ */
