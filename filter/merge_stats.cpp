#include "cado.h"
#include "macros.h"
#include "omp_proxy.h"
#include "merge_stats.h"
#include <vector>
#include <iostream>
#include "fmt/format.h"

static std::vector<std::vector<unsigned long>> stats;

void add(std::vector<unsigned long>& t, unsigned long weight, unsigned long count = 1)
{
    for ( ; (size_t) weight >= t.size() ; )
        t.emplace_back();
    t[weight] += count;
}

void merge_stat_add1(int id, int weight)
{
    ASSERT_ALWAYS((size_t) id < stats.size());
    add(stats[id], weight);
}

void merge_stat_prepare(int nthreads)
{
    stats.assign(nthreads, std::vector<unsigned long>());
}

void merge_stat_print_stats()
{
    std::ostream & os = std::cout;
    std::vector<unsigned long> sum;
    for(auto const & s : stats) {
        for(size_t i = 0 ; i < s.size() ; i++) {
            add(sum, i, s[i]);
        }
    }

    for(size_t i = 2 ; i < sum.size() ; i++)
        os << fmt::format(FMT_STRING(" {}:{}"), i, sum[i]);
    os << "\n";
}
